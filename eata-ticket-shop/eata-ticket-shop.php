<?php
/*
Plugin Name: EATA Wordpress Ticketshop
Plugin URI: https://www.entradasatualcance.com/
Description: Use it to include your ticketshop iframe in your Wordpress site
Author: Jose Gilberto Mullor
Author URI: http://www.twitter.com/jgmullor
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Version: 0.1

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_shortcode( 'eatashop', 'eata_shop_launch' );

function eata_random($length=4)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string = "";
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }

    return $string;
}
	
function eata_shop_launch( $matches, $atts, $url, $rawattr=null ) {
	$random = eata_random();
    $event = preg_replace('/[^a-zA-Z0-9_-]/', '', $matches["event"]);
    $width = (empty($matches["width"]))? "600px" : preg_replace('/[^0-9]/', '', $matches["width"]) . "px";
    $height = (empty($matches["height"]))? "700px" : preg_replace('/[^0-9]/', '', $matches["height"]) . "px";

	$embed = "<div id='entradasatualcanceShop$random'><img src='" . plugins_url( "eata-ticket-shop/img/loading.gif") . "' /></div>";
    $embed .= "<script language='javascript' type='text/javascript' src='https://www.entradasatualcance.com/$event/iframe/script/?wpver=0.1&width=$width&height=$height&model=facebook&transparent=true&r=$random&" . $_SERVER["QUERY_STRING"]. "'></script>";

	return apply_filters( 'eata', $embed, $matches, $atts, $url, $rawattr  );
}